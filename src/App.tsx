import { Routes as Router, Route, BrowserRouter } from "react-router-dom";
import { Home } from "./pages/Home";
import { NewRoom } from "./pages/NewRoom";
import { AuthContextProvider } from './context/AuthContext'

function App() {
    return (
        <BrowserRouter>
            <AuthContextProvider>
                <Router>
                    <Route path="/" element={<Home />} />
                    <Route path="/rooms/new" element={<NewRoom />} />
                </Router>
            </AuthContextProvider>
        </BrowserRouter>
    );  
}

export default App;